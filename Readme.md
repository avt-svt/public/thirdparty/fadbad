The folder fadbad-2.1 contains FADBAD++ version 2.1, downloaded on January 22, 2019 from www.fadbad.com/fadbad.html
For license information, please refer to the files therein.

This fadbad version has been tested for the following configurations:
	- Windows 7 using Microsoft Visual Studio 2017
	- Red Hat Linux 8.2.1  using gcc 8.2.1 
