/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "fadbad.h"
#include "fadiff.h"
#include "badiff.h"
#include "tadiff.h"

#include <iostream>

int main() {

	fadbad::FTypeName<double> x=1.;
	x.diff(0,1);
	fadbad::FTypeName<double> f = 5 + 3*(pow(x,2)+x);
	
	std::cout << std::endl << "f := 5 + 3*(x^2+x)" << std::endl << std::endl;
	std::cout << "f(1) = " << f.val() << std::endl;
	std::cout << "df/dx (x=1) =  " << f.d(0) << std::endl << std::endl;
	std::cout << "Fadbad++ seems to work..." << std::endl;
	std::cout << "Note that this test is merely intended to test compilation / setup of the repository and CmakeLists.txt" << std::endl;
	std::cout << "Further tests can be found in fadbad-2.1/test" << std::endl;
	
	return 0;
	
}